#!/bin/bash

TIMESTAMP=`date "+%Y.%m.%d-%H.%M.%S"`
TAG_NAME= 'base'

if [ -z "$1" ] && [ -z "$2" ]
  then
    echo "Required access and secret keys. Usage: ./run-packer.sh access-key secret-key"
    exit 1
fi

ACCESS_KEY=$1
SECRET_KEY=$2

echo "Building AMI for version ..."
packer.io build \
	-var "ami_name=$TAG_NAME CentOS7 $TIMESTAMP" \
	-var "aws_access_key=$ACCESS_KEY" \
	-var "aws_secret_key=$SECRET_KEY" \
	./base-instance.json
