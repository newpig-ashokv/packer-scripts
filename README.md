# Base Instance Packer Script

This directory contains the files necessary to create the AMI for base instances. To create the image execute Packer through the `run-packer.sh` script. Note that the Packer executable must be in your path. You may need to set your AWS access/secret keys if executing without an IAM role.

The AMI used to create the base instances is an encrypted CentOS7. This image was made from the CentOS7 AMI from the AWS Marketplace.